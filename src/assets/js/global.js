import router from '@/router'

export default({
	//后退
	back:function(){
		window.history.go(-1);
	},
	//百分比
	percentFormat:function(i){
		if(i == 0 || i == 'NaN'){
			return 0;
		}
		else{
			return (Math.round(i * 10000) / 100).toFixed(1) + '%';
		}
	},
	//页面跳转
	goto:function(path){
		router.push(path);
	},
	//设置cookie
	setCookie:function(name,value){
		var expdate = new Date();   //初始化时间
	    expdate.setTime(expdate.getTime() + 3600 * 24 * 30);   //时间
	    document.cookie = name+"="+value+";expires="+expdate.toGMTString()+";path=/";
	},
	//获取cookie
	getCookie:function(c_name){
		if (document.cookie.length>0){
			let c_start=document.cookie.indexOf(c_name + "=")
			if (c_start!=-1){
				c_start=c_start + c_name.length+1
				let c_end=document.cookie.indexOf(";",c_start)
				if (c_end==-1) c_end=document.cookie.length
					return unescape(document.cookie.substring(c_start,c_end))
				}
			}
		return ""
	},
	//删除cookie
	delCookie:function(c_name){
		this.setCookie(c_name, "", -1);
	},
	//验证手机号（非必填）
	checkPhone:function(phone){
		var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0-9]{1})|(15[0-3]{1})|(15[4-9]{1})|(18[0-9]{1})|(199))+\d{8})$/;
		if (phone.length != 11) {
			return false;
		}
		else if (!myreg.test(phone)) {
			return false;
		}
		else{
			return true;
		}
	},
	//获取url参数
	getParamString:function(name){
		var paramUrl = window.location.search.substr(1);
		var paramStrs = paramUrl.split('&');
		var params = {};
		for(var index = 0; index < paramStrs.length; index++) {
			params[paramStrs[index].split('=')[0]] = decodeURI(paramStrs[index].split('=')[1]);
		}
		return params[name];
	},
	//格式化时间选择器
	formatter:function(type, value){
		if (type === "year") {
			return `${value}年`;
		} else if (type === "month") {
			return `${value}月`;
		} else if (type === "day") {
			return `${value}日`;
		} else if (type === "hour") {
			return `${value}时`;
		} else if (type === "minute") {
			return `${value}分`;
		}
		return value;
	},
	//中国标准时间转正常时间
	formatDateTime : function (date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		var h = date.getHours();
		h=h < 10 ? ('0' + h) : h;
		var minute = date.getMinutes();
		minute = minute < 10 ? ('0' + minute) : minute;
		var second=date.getSeconds();
		second=second < 10 ? ('0' + second) : second;
		return y + '-' + m + '-' + d;
	},
	//开始时间必须小于结束时间
	checkTimer:function(startTime,endTime){
		var arr1 = startTime.split("-");
		var arr2 = endTime.split("-");
		var date1=new Date(parseInt(arr1[0]),parseInt(arr1[1])-1,parseInt(arr1[2]),0,0,0);
		var date2=new Date(parseInt(arr2[0]),parseInt(arr2[1])-1,parseInt(arr2[2]),0,0,0);
		//失败：开始时间>结束时间
		if(date1.getTime()>date2.getTime()) {
			   return false;
		 }
		 //成功：开始时间<结束时间
		 else{
			 return true;
		 }
	},
	//时间戳转时间
	toDate:function(value){
		let date = new Date(value);
		let y = date.getFullYear();
		let MM = date.getMonth() + 1;
		MM = MM < 10 ? ('0' + MM) : MM;
		let d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		let h = date.getHours();
		h = h < 10 ? ('0' + h) : h;
		let m = date.getMinutes();
		m = m < 10 ? ('0' + m) : m;
		let s = date.getSeconds();
		s = s < 10 ? ('0' + s) : s;
		return y + '-' + MM + '-' + d;
	},
})
