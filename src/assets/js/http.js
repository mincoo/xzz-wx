import axios from 'axios'
import { Toast } from 'vant';
import common from './global.js'

//全局请求地址
//axios.defaults.baseURL = 'http://192.168.0.136:8091/wechat' //内网
axios.defaults.baseURL = 'http://xzza.utools.club/wechat' //内网
// axios.defaults.baseURL = 'http://api.xuezhuangzhuang.cn/wechat' //正式

//超时时间
axios.defaults.timeout = 8000

//请求前
axios.interceptors.request.use(config => {
	//显示加载中
	Toast.loading({
		duration: 0,
		forbidClick: true,
		message: '加载中...'
	});
	//统一在http请求的header都加上token
	if (common.getCookie('token')) {
		config.headers.token = common.getCookie('token');
	}
	return config
},error=>{
	Toast('请求超时');
})

//请求后
axios.interceptors.response.use(
    response => {
		//关闭加载中
		Toast.clear();
        return response;
    },
    error => {
		//请求失败
		Toast('服务器请求错误');
		return error;
});
