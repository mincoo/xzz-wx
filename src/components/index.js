import cmsProgress from './progress/progress.vue'


const cmsComponents = {
    install: function (Vue) {
        Vue.component('cmsProgress', cmsProgress);
    }
}

// 导出组件
export default cmsComponents