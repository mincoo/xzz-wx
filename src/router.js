import Vue from 'vue'
import Router from 'vue-router'
import view from '@/components/layout/view'

Vue.use(Router)

export default new Router({
	//mode: 'history',
	base: process.env.BASE_URL,
	mode:'history',
	routes: [
		//登录
		{
			path: '/login',
			component: () => import('./pages/login/login.vue')
		},
		//忘记密码
		{
			path: '/forget',
			component: () => import('./pages/login/forget.vue')
		},
		//设置新密码
		{
			path: '/new',
			component: () => import('./pages/login/new.vue')
		},
		//首页
		{
			path: '/',
			component: () => import('./pages/index/index.vue')
		},
		//单项
		{
			path: '/single',
			component: () => import('./pages/tizhi/single.vue')
		},
		//综合
		{
			path: '/syn',
			component: () => import('./pages/tizhi/syn.vue')
		},
		//历史分析
		{
			path: '/history',
			component: () => import('./pages/tizhi/history.vue')
		},
		//体质报告
		{
			path: '/report',
			component: () => import('./pages/tizhi/report.vue')
		},
		//视力报告
		{
			path: '/eyeReport',
			component: () => import('./pages/eye/report.vue')
		},
		//我的
		{
			path: '/my',
			component: () => import('./pages/my/my.vue')
		},
		//视力
		{
			path: '/eye',
			component:view,
			children:[
				//视力 - 首页
				{
					path:'/',
					component:() => import('./pages/eye/eye.vue')
				},
				//视力 - 历史
				{
					path:'history',
					component:() => import('./pages/eye/history.vue')
				},
			]
		},
		//公告详情
		{
			path: '/note',
			component: () => import('./pages/news/detail.vue')
		},
	]
})
