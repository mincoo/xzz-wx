module.exports = {
	lintOnSave: false,
	baseUrl:'./',
	devServer: {
		port: 9001,
		compress: true,
		disableHostCheck: true,   // That solved it
	}
}